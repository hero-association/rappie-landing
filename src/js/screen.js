$(document).ready(function() {
    $('.deliverToYou .form .btn').click(function() {
        var valid_numbers = [3011, 3012, 3013, 3014, 3015, 3016, 3021, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3039, 3051, 3052, 3061, 3062, 3071];
        var reg_postcode = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
        var postcode = $(this).parent().children('input').val();
        $("#zip").val(postcode);
        
        if (!reg_postcode.test(postcode))
            $.alertable.alert('Het lijkt erop dat je een ongeldige postcode hebt ingevoerd.')
        else
        {
            var number = parseInt(postcode.substring(0, 4));
            if (valid_numbers.indexOf(number) < 0)
            {
                $('.popupDelivery .yes').addClass('hide');
                $('.popupDelivery .thanks').addClass('hide');
                $('.popupDelivery .no').removeClass('hide');
            }
            else {
                $('.popupDelivery .no').addClass('hide');
                $('.popupDelivery .thanks').addClass('hide');
                $('.popupDelivery .yes').removeClass('hide');
            }
            $('.popupDelivery').removeClass('hide');

            $.ajax({
                type: 'POST',
                url: 'log_zip.php', 
                data: { zipcode: postcode }
            })
        }
    });
    
    $("#naam, #email").keyup(function()
    {
        var reg_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (reg_email.test(String($("#email").val()).toLowerCase()) && $("#naam").val().length > 0)
        {
            $("#add_to_list").removeClass('disabled');
            $(".error").addClass('hide');
        }
        else
        {
            $("#add_to_list").addClass('disabled');
        }
    });

    $("#add_to_list").click(function() {
        if ($(this).hasClass("disabled"))
        {
            $(".error").removeClass('hide');
            return false;
        }
        else
            $("#form_register").submit();
    });

    $('.popupDelivery .value .close').click(function()
    {
        $('.popupDelivery').addClass('hide');
    });
    
    $('.faq .questions .question').click(function()
    {
        $(this).toggleClass('active');
        $(this).children('.answer').toggleClass('hide');
    });

    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    if($.urlParam('aangemeld') == 'ja');
    {
        $('.popupDelivery .no').addClass('hide');
        $('.popupDelivery .yes').addClass('hide');
        $('.popupDelivery .thanks').removeClass('hide');
        $('.popupDelivery').removeClass('hide');
    }
});